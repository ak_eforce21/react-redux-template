# Setup

- npm
- webpack
- typescript
- react

# React

- jsx, components
- props, state, data binding
- class vs hooks
- render insights
- component lifecycle

# Redux

- store, reducers, actions
- redux devtools
- typescript-fsa
- middleware

# Redux-Saga

- generators
- side-effects
- async action flows
