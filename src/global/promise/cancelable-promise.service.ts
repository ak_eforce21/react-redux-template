export interface CancelablePromise<T> {
  promise: Promise<T>;
  cancel(): void;
}

export namespace CancelablePromiseService {
  export function makeCancelable<T>(promise: Promise<T>): CancelablePromise<T> {
    let hasCanceled_ = false;
    return {
      promise: new Promise((resolve, reject) =>
        promise.then((r) => (hasCanceled_ ? reject({ isCanceled: true }) : resolve(r))),
      ),
      cancel() {
        hasCanceled_ = true;
      },
    };
  }
}
