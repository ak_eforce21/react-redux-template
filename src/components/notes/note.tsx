import React, { memo } from 'react';

import { StyledText } from '../widget/styled-text';

export const Note = memo(function Note(props: { text: string }) {
  return (
    <li>
      <StyledText>{props.text}</StyledText>
    </li>
  );
});
