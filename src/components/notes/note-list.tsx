import React, { useEffect, useState } from 'react';

import { StyledText } from '../widget/styled-text';
import { Note } from './note';
import { NoteInput } from './note-input';
import { NoteService } from './note.service';

export function NoteList() {
  const [notes, setNotes] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    let isMounted = true;
    NoteService.loadNotes().then((loadedNotes) => {
      if (!isMounted) {
        return;
      }
      setNotes(loadedNotes);
      setIsLoading(false);
    });
    return () => {
      isMounted = false;
    };
  }, []);
  if (isLoading) {
    return <StyledText>loading notes...</StyledText>;
  }
  return (
    <>
      <ul>
        {notes.map((note, i) => (
          <Note text={note} key={i} /> // index is not a good react-key
        ))}
      </ul>
      <NoteInput
        onAddNote={(text: string) => {
          setNotes([...notes, text]);
        }}
      />
    </>
  );
}
