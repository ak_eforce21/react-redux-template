import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { Help } from '../help/help';
import { Login } from '../login/login';
import { NavigationBar, Tab } from '../navigation/navigation-bar';
import { NoteList } from '../notes/note-list';
import { Settings } from '../settings/settings';
import { AppState } from './app.reducer';

export function App() {
  const isLoggedIn = useSelector((state: AppState) => state.login.loggedInUser !== null);
  const [activeTab, setActiveTab] = useState<Tab>(Tab.NOTES);
  if (!isLoggedIn) {
    return <Login />;
  }
  return (
    <>
      <NavigationBar setActiveTab={setActiveTab} />
      {renderContent(activeTab)}
    </>
  );
}

function renderContent(activeTab: Tab): JSX.Element {
  switch (activeTab) {
    case Tab.NOTES:
      return <NoteList />;
    case Tab.SETTINGS:
      return <Settings />;
    case Tab.HELP:
      return <Help />;
  }
}
