import { combineReducers } from 'redux';

import { loginReducer, LoginState } from '../login/login.reducer';
import { SettingsState } from '../settings/settings.model';
import { settingsReducer } from '../settings/settings.reducer';

export interface AppState {
  login: LoginState;
  settings: SettingsState;
}

export const appReducer = combineReducers({
  login: loginReducer,
  settings: settingsReducer,
});
