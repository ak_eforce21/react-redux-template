import { CancelablePromise, CancelablePromiseService } from '../../global/promise/cancelable-promise.service';
import { Theme, ThemeSettings } from './settings.model';

export namespace SettingsApiService {
  export function load(): CancelablePromise<ThemeSettings> {
    return CancelablePromiseService.makeCancelable(
      new Promise((resolve) =>
        setTimeout(() => {
          resolve({ defaultTheme: Theme.RED, themeOptions: Object.values(Theme) });
        }, 1000),
      ),
    );
  }
}
