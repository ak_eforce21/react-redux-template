import { Middleware } from 'redux';
import { AnyAction } from 'typescript-fsa';

import { SettingsPersistenceService } from './persistence.service';
import { SettingsActions } from './settings.actions';

export const settingsPersistenceMiddleware: Middleware = (api) => (next) => (action: AnyAction) => {
  if (SettingsActions.setTheme.match(action)) {
    SettingsPersistenceService.setTheme(action.payload.theme);
  }
  return next(action);
};
