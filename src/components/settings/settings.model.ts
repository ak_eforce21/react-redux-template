export interface SettingsState {
  theme?: Theme;
}

export enum Theme {
  RED = 'red',
  GREEN = 'green',
  BLUE = 'blue',
}

export interface ThemeSettings {
  defaultTheme: Theme;
  themeOptions: Theme[];
}
