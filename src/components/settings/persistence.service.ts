import { Theme } from './settings.model';

const THEME_KEY = 'theme';

export namespace SettingsPersistenceService {
  export function setTheme(theme: Theme) {
    localStorage.setItem(THEME_KEY, theme);
  }

  export function getTheme(): Theme | null {
    return localStorage.getItem(THEME_KEY) as Theme | null;
  }
}
