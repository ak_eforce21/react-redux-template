import actionCreatorFactory from 'typescript-fsa';

import { Theme } from './settings.model';

const actionCreator = actionCreatorFactory('SETTINGS');

export namespace SettingsActions {
  export const setTheme = actionCreator<{ theme: Theme }>('SET_THEME');
}
