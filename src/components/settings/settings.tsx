import React, { Component, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { AppState } from '../app/app.reducer';
import { StyledText } from '../widget/styled-text';
import { SettingsApiService } from './api.service';
import { SettingsActions } from './settings.actions';
import { Theme } from './settings.model';

export function Settings() {
  const appliedTheme = useSelector((state: AppState) => state.settings.theme);
  const [isLoaded, setIsLoaded] = useState(false);
  const [theme, setTheme] = useState(appliedTheme);
  const [themeOptions, setThemeOptions] = useState<Theme[]>([]);
  const dispatch = useDispatch();

  useEffect(() => {
    const loadTask = SettingsApiService.load();
    loadTask.promise
      .then(({ themeOptions, defaultTheme }) => {
        setTheme(theme || defaultTheme);
        setThemeOptions(themeOptions);
        setIsLoaded(true);
      })
      .catch((reason) => {
        if (!reason.isCanceled) {
          throw reason;
        }
      });
    return () => {
      loadTask.cancel();
    };
  }, []);

  if (!isLoaded) {
    return <StyledText>loading themes...</StyledText>;
  }

  function onSetTheme(event: React.ChangeEvent<HTMLSelectElement>) {
    const theme = event.target.value as Theme;
    setTheme(theme);
  }

  function onApply() {
    if (theme) {
      dispatch(SettingsActions.setTheme({ theme }));
    }
  }

  return (
    <div>
      <label>
        <StyledText>Theme</StyledText>
      </label>
      <select value={theme!} onChange={onSetTheme}>
        {themeOptions.map((themeOption) => (
          <option key={themeOption}>{themeOption}</option>
        ))}
      </select>
      <p style={{ color: theme! }}>Sample</p>
      <button onClick={onApply}>Apply</button>
    </div>
  );
}
