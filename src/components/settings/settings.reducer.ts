import { AnyAction } from 'typescript-fsa';

import { SettingsPersistenceService } from './persistence.service';
import { SettingsActions } from './settings.actions';
import { SettingsState } from './settings.model';

const initialState: SettingsState = {
  theme: SettingsPersistenceService.getTheme() || undefined,
};

export function settingsReducer(
  state: Readonly<SettingsState> = initialState,
  action: AnyAction,
): SettingsState {
  if (SettingsActions.setTheme.match(action)) {
    return { ...state, theme: action.payload.theme };
  }
  return state;
}
