import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { AppState } from '../app/app.reducer';
import { LoginState } from '../login/login.reducer';
import { ConversationalForm, ConversationGenerator } from '../widget/conversational-form';

export function Help() {
  const user = useSelector((state: AppState) => state.login.loggedInUser);
  const generator = useMemo(() => conversation(user), []);
  return <ConversationalForm generator={generator} />;
}

function* conversation(user: LoginState['loggedInUser']): ConversationGenerator {
  const problem = yield* getValidProblem();
  const contact = yield 'Sorry for that. How can I contact you?';
  return `Dear ${user}, I will investigate your problem (${problem}) and contact you (${contact}) asap!`;
}

function* getValidProblem(): ConversationGenerator {
  let problem = yield 'How can I help?';
  while (!problem) {
    problem = yield 'Please describe your problem!';
  }
  return problem;
}
