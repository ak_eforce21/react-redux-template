import React from 'react';
import { useDispatch } from 'react-redux';

import { LoginActions } from '../login/login.actions';
import { StyledText } from '../widget/styled-text';

export enum Tab {
  NOTES = 'notes',
  SETTINGS = 'settings',
  HELP = 'help',
}

export function NavigationBar(props: { setActiveTab: (t: Tab) => void }) {
  const dispatch = useDispatch();
  return (
    <div>
      <StyledText>Not another To-Do-App</StyledText>
      {Object.values(Tab).map((t) => (
        <button key={t} onClick={() => props.setActiveTab(t)}>
          {t}
        </button>
      ))}
      <button
        onClick={() => {
          dispatch(LoginActions.requestLogout());
        }}
      >
        logout
      </button>
    </div>
  );
}
