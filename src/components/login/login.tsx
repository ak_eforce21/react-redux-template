import React, { Component } from 'react';
import { connect } from 'react-redux';

import { StyledText } from '../widget/styled-text';
import { LoginActions } from './login.actions';

interface LoginProps {
  login: (user: string) => void;
}

interface LoginState {
  username: string;
}

class LoginComponent extends Component<LoginProps, LoginState> {
  constructor(props: LoginProps) {
    super(props);
    this.state = { username: '' };
    this.onLogin = this.onLogin.bind(this);
  }

  onLogin() {
    this.props.login(this.state.username);
  }

  render() {
    return (
      <div>
        <StyledText>please log in</StyledText>
        <br />
        <StyledText>username:</StyledText>
        <br />
        <input
          type="text"
          value={this.state.username}
          onChange={(e) => this.setState({ username: e.target.value })}
        />
        <button onClick={this.onLogin}>login</button>
      </div>
    );
  }
}

export const Login = connect(null, {
  login: (username: string) => LoginActions.requestLogin({ username }),
})(LoginComponent);
