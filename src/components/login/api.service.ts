export namespace LoginApiService {
  export function login(username: string): Promise<string> {
    return new Promise((resolve) => {
      setTimeout(() => resolve(username), 1000);
    });
  }

  export function logout(): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(() => resolve(), 1000);
    });
  }
}
