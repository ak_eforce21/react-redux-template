import { call, put, takeEvery, takeLeading } from 'redux-saga/effects';

import { LoginApiService } from './api.service';
import { LoginActions } from './login.actions';
import { LoginPersistenceService } from './persistence.service';

export function* loginSaga() {
  yield takeEvery(LoginActions.loggedIn, (action) => {
    LoginPersistenceService.login(action.payload.user);
  });
  yield takeEvery(LoginActions.loggedOut, () => {
    LoginPersistenceService.logout();
  });
  yield takeLeading(LoginActions.requestLogin, requestLogin);
  yield takeLeading(LoginActions.requestLogout, requestLogout);
}

function* requestLogin(action: ReturnType<typeof LoginActions.requestLogin>) {
  // could also fail
  const user: string = yield call(LoginApiService.login, action.payload.username);
  yield put(LoginActions.loggedIn({ user }));
}

function* requestLogout() {
  // could also fail
  yield call(LoginApiService.logout);
  yield put(LoginActions.loggedOut());
}
