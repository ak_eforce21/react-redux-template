import React, { ReactNode } from 'react';
import { useSelector } from 'react-redux';

import { AppState } from '../app/app.reducer';

export function StyledText(props: { children?: ReactNode }) {
  const theme = useSelector((state: AppState) => state.settings.theme);
  return <span style={{ color: theme }}>{props.children}</span>;
}
