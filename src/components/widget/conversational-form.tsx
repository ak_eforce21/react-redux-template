import React, { useMemo, useState } from 'react';

export type ConversationGenerator = Generator<string, string, string>;

export function ConversationalForm({ generator }: { generator: ConversationGenerator }) {
  const initialMessage = useMemo(() => generator.next(), []);
  const [conversation, setConversation] = useState([
    { text: initialMessage.value, isFromUser: false },
  ]);
  const [isFinished, setIsFinished] = useState(initialMessage.done);
  const [input, setInput] = useState('');

  function onSend() {
    const answer = generator.next(input);
    setConversation([
      ...conversation,
      { text: input, isFromUser: true },
      { text: answer.value, isFromUser: false },
    ]);
    setInput('');
    if (answer.done) {
      setIsFinished(true);
    }
  }

  return (
    <div>
      {conversation.map(({ text, isFromUser }, i) => (
        <p key={`message-${i}`} style={{ width: '90vw', textAlign: isFromUser ? 'right' : 'left' }}>
          {text}
        </p>
      ))}
      {!isFinished && (
        <>
          <input value={input} onChange={(e) => setInput(e.target.value)} />
          <button onClick={onSend}>send</button>
        </>
      )}
    </div>
  );
}
